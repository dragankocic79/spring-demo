package com.example.demo.controller;

import com.example.demo.entity.UserRequest;
import com.example.demo.entity.UserResponse;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    @PostMapping
    public ResponseEntity<UserResponse> create(@RequestBody UserRequest userRequest) {
        UserResponse response = userService.save(userRequest);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> get(@PathVariable Long id) {
        UserResponse response = userService.get(id);
        return ResponseEntity.ok(response);
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}

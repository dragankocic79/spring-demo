package com.example.demo.entity;

import lombok.Data;

import javax.validation.constraints.Min;

@Data
public class UserRequest {

    @Min(3)
    private String firstName;

    private String lastName;

    private String password;
}

package com.example.demo.service;

import com.example.demo.entity.UserRequest;
import com.example.demo.entity.UserResponse;

public interface UserService {

    UserResponse save(UserRequest user);

    UserResponse get(Long id);

}

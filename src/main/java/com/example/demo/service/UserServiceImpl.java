package com.example.demo.service;

import com.example.demo.entity.User;
import com.example.demo.entity.UserRequest;
import com.example.demo.entity.UserResponse;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    private UserRepository userRepo;

    @Override
    public UserResponse save(UserRequest user) {

        User saveUser = mapToSave(user);
        User savedUser = userRepo.save(saveUser);
        UserResponse response = mapToResponse(savedUser);
        return response;
    }

    @Override
    public UserResponse get(Long id) {
        User user = userRepo.findById(id).get();
        UserResponse response = mapToResponse(user);
        return response;
    }

    private UserResponse mapToResponse(User savedUser) {
        UserResponse result = new UserResponse();
        result.setId(savedUser.getId());
        result.setFirstName(savedUser.getFirstName());
        result.setLastName(savedUser.getLastName());
        return result;
    }

    private User mapToSave(UserRequest user) {
        User result = new User();
        result.setFirstName(user.getFirstName());
        result.setLastName(user.getLastName());
        result.setPassword(user.getPassword());
        return result;
    }

    @Autowired
    public void setUserRepo(UserRepository userRepo) {
        this.userRepo = userRepo;
    }
}
